import java.util.Arrays

fun twoSum(nums: IntArray, target: Int): IntArray {
    
    /*for (i in 0 until nums.size) {
        if (nums[i]  == target) {
            return intArrayOf(i)
        }
    }*/
    
    for (i in 0 until nums.size) {
        for (j in i + 1 until nums.size) {
            if (nums[i] + nums[j] == target) {
                return intArrayOf(i, j)
            }
        }
    }
    
    return intArrayOf(0, 0)
}

fun plusOne(digits: IntArray): IntArray {
    for (i in digits.size-1 downTo 0) {
        if(digits[i] != 9) {
            digits[i] = digits[i] + 1;
            return digits;
        }
        digits[i] = 0;      
    }   
    
    var nums = IntArray(digits.size + 1)
    nums[0] = 1;
    return nums;  
   
}

fun main() {
    println("** Práctica Two Sum **")
    val from:Int = 0
    val to:Int = 20
    val target:Int = 9
    var nums = IntArray(20) {(from..to).random() }
    //val nums = intArrayOf(2, 7, 11, 15)   
    println("Nums: ${Arrays.toString(nums)}")
    println("Target: ${target}")
    println("Result: ${Arrays.toString(twoSum(nums,target))}")
    println("** Práctica Plus One **")
    val digits = intArrayOf(9)   
    println("Digits: ${Arrays.toString(digits)}")
    println("Result: ${Arrays.toString(plusOne(digits))}")
}
